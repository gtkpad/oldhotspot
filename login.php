<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type");
// header('Content-Type: application/json; charset=utf-8');

require "include/functions.php";
require "include/routeros_api.class_v1.6.php";
require_once "include/classes/mikrotik.class.php";
require_once "include/classes/cliente.class.php";
require_once "include/classes/log.class.php";
$ip = $_SERVER['REMOTE_ADDR'];
$mikrotik = new Mikrotik($ip);


if(isset($_POST['mac']) && !empty($_POST['mac']) && $ip == $mikrotik->nasname){
	
	
	$mac = $_POST['mac'];
	// $mac =  "F0:D7:AA:B6:C2:3D";  
	$cliente = new Cliente();
	$log = new Log($ip);
  	$valido = $cliente->verificaMac($mac, $mikrotik->empresa_id);
   

  	if($valido != 0){
		if ($cliente->verificaMacBloqueado($mac, $mikrotik->empresa_id) == false){	
			$clientedb = $cliente->buscaClienteMac($mac);
			$nome = $clientedb['nome'];
			$user = $clientedb['usuario'];
			$user = $user."@".$mac;
			$id = $clientedb['id'];
			$password = $clientedb['senha'];
			// $array_cliente = array('nome' => $nome, 'user' => $user, 'pass' => $password);
			$json = json_encode(array('nome' => $nome, 'user' => $user, 'pass' => $password));
			echo $json; 
				
			$mikrotik->addUserRb($user, $password);
			$log->addLog($mac, $mikrotik->empresa_id);
			$cliente->verificaAcessoIsNull($id);
		} else{
			echo "44";
		}	

		
  	} else{
  		echo "0";
  	}

  
} else{
	header("Location: index.html");
}


?>
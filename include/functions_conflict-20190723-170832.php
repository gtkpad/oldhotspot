<?php
function limpavar($var)
{
//$var = preg_replace('/[^[:alnum:]_]/', '',$var);
$var = preg_replace("/[^0-9,]/", "", $var);
$var = trim($var);//limpa espaços vazio
$var = strip_tags($var);//tira tags html e php
$var = addslashes($var);//Adiciona barras invertidas a uma string
return $var;
}

function limpavarstr($var){
$var = preg_replace('/[^[:alnum:]_\:]/', '',$var);
//$var = preg_replace("/[^0-9]/", "",$var);
$var = trim($var);//limpa espaços vazio
$var = strip_tags($var);//tira tags html e php
$var = addslashes($var);//Adiciona barras invertidas a uma string
return $var;
}

function limpavaremail($var){
$var = preg_replace('/[^[:alnum:]_\@\.]/', '',$var);
//$var = preg_replace("/[^0-9]/", "",$var);
$var = trim($var);//limpa espaços vazio
$var = strip_tags($var);//tira tags html e php
$var = addslashes($var);//Adiciona barras invertidas a uma string
return $var;
}

function limpavarstrname($var){
$var = preg_replace('/[^[:alnum:]_]/', '',$var);
//$var = preg_replace("/[^0-9]/", "",$var);
// $var = trim($var);//limpa espaços vazio
$var = strip_tags($var);//tira tags html e php
$var = addslashes($var);//Adiciona barras invertidas a uma string
return $var;
}


class Log{
	private $pdo;
	private $api;
	private $nasname = "186.250.168.241";

	private function __construct(){
		$this->$pdo = new PDO("mysql:dbname=bc_brainstorm;host=localhost", "bc_brainstorm", "eRgWyvldaQ");
		$this->$api = new routeros_api();
	}



	private function getInfoArray($array, $mac, $info){
		foreach ($array as $dispositivo) {
	            foreach ($dispositivo as $item => $valor_item) {
	                   if($valor_item == $mac){
	                    $macadd = $valor_item;
	                    foreach ($dispositivo as $item2 => $valor_item2) {
	                       if($item2 == $info){
	                        $retorna = $valor_item2;
	                       }
	                    }
	                
	                }
	            
	            }
	        }
	    return $retorna;
	}
	private function getMacConectado($array, $mac){
		foreach ($array as $dispositivo) {
	        foreach ($dispositivo as $item => $valor_item) {
	            if($valor_item == $mac){
	            	return 1;                
	            } else{
	            	return 0;
	            }
	            
	        }
	    }
	}

	private function atualizaUptime($mac){
		$users = $this->apiGetUsersInfo();
		// $hosts = $this->apiGetHostsInfo();
		// $address = $this->apiGetAddressInfo()
		// $active = $this->apiGetActiveInfo();
		$uptime = $this->getInfoArray($users, $mac, "uptime");
		// $ip = $this->getInfoArray($active, $mac, "address");
		// $ip_rb = getInfoArray($address, "ether1", "address");
		$sql = "UPDATE log SET tempo = :uptime";
		$this->$pdo->prepare($sql);
		$sql->bindValue(":uptime", $uptime);
		$sql->execute();


	}


	private function apiGetAddressInfo()
	{
		if($this->$API->connect($this->$nasname, $user_rb, $pass_rb)) {

	        $this->$API->write('/ip/address/print', false);
		    $array = $API->read();

		    $this->$API->disconnect();

		    if(!empty($array)){
		    	return $array;
		    } else{
		    	return 0;
		    }
		}
	}


	private function apiGetUsersInfo()
	{
		if($this->$API->connect($this->$nasname, $user_rb, $pass_rb)) {

	        $this->$API->write('/ip/hotspot/user/print', false);
		    $array = $API->read();

		    $this->$API->disconnect();

		    if(!empty($array)){
		    	return $array;
		    } else{
		    	return 0;
		    }
		}
	}
	

	private function apiGetHostsInfo()
	{
		if($this->$API->connect($this->$nasname, $user_rb, $pass_rb)) {

	        $this->$API->write('/ip/hotspot/hosts/print', false);
		    $array = $API->read();

		    $this->$API->disconnect();

		    if(!empty($array)){
		    	return $array;
		    } else{
		    	return 0;
		    }
		}
	}

	public function apiGetActiveInfo()
	{
		if($this->$API->connect($this->$nasname, $user_rb, $pass_rb)) {

	        $this->$API->write('/ip/hotspot/active/print', false);
		    $array = $API->read();

		    $this->$API->disconnect();

		    if(!empty($array)){
		    	return $array;
		    } else{
		    	return 0;
		    }
		}
	}

	public function addConexao($mac){

	}


	public function rotina($mac){
		$data = $this->bdListaConexaoAtiva();
		if($data != 0){
			foreach ($data as $conexao) {
				$mac = $conexao['mac'];

			}
		}
	}

	private function bdAddConexao($cod_cliente, $mac, $ip_cliente, $ip_rb){
		$sql = "INSERT INTO log (cod_cliente, ip_equip, mac, ip_rb, ativo) VALUES (:cod_cliente, :ip_equip, :mac, :ip_rb, 1)";
		$sql = $this->$pdo->prepare($sql);
		$sql->bindValue(":cod_cliente", $cod_cliente);
		$sql->bindValue(":ip_equip", $ip_equip);
		$sql->bindValue(":mac", $mac);
		$sql->bindValue(":ip_rb", $ip_rb);
		$sql->execute();
	}


	public function setInativo($mac){
		$valido = $this->verificaMacAtivo($mac);
		if($valido){
			$sql = "UPDATE log SET ativo = 0 WHERE mac = :mac AND ativo = 1";
			$sql = $this->$pdo->prepare($sql);
			$sql->bindValue(":mac", $mac);
			$sql->execute();
		} else{
			return 0;
		}		
	}

	public function bdVerificaMacAtivo($mac){
		$sql = "SELECT * FROM log WHERE ativo = 1 AND mac = :mac";
		$sql = $this->$pdo->prepare($sql);
		$sql->bindValue(":mac", $mac);
		$sql->execute();
		if($sql->rowCount() > 0){
			return 1;
		} else{
			return 0;
		}
	}
	private function verificaMacAtivoRb($mac){
		$active = $this->apiGetActiveInfo();
		if(!empty($active)){
			$retorno = $this->getMacConectado($active, $mac);
			if($retorno){
				return 1;
			} else{
				return 0;
			}
		}
	}
	private function verificaConexao($mac){
		$ativo_bd = $this->bdVerificaMacAtivo($mac);
		$ativo_rb =$this->verificaMacAtivoRb($mac);
		if($ativo_bd && !$ativo_rb){
			


			
		}

	}

	private function bdListaConexaoAtiva(){
		$sql = "SELECT * FROM log WHERE ativo = 1";
		$sql = $this->$pdo->prepare($sql);
		$sql->execute();
		if($sql->rowCount() > 0){
			$retorno = $sql->fetchALL();
		} else{
			return 0;
		}
	}

}

/*
 
 */
class Cliente extends Log
{
	private $cliente;


	public function buscaClienteMac($mac){
		$valida = $this->bdVerificaMac($mac);
		if($valida != 0){
			$retorno = $this->bdVerificaClienteMac($mac);
			$this->$cliente = $retorno; 
			return $retorno;		
		} else{
			return "Mac não cadastrado";
		}
	
	}
	public function addAcesso(){
		$retorno = $this->bdAddAcesso($id);
		if ($retorno) {
			return 1;
		} else{
			return 0;
		}
	}

	public function verificaMac($mac){
		if(strlen($mac) == 17){
			if($this->bdVerificaMac($mac) != 0){
				return 1;
			} else{
				return 0;
			}
		} else{
			return 0;
		}
	}

	public function cadastraCliente($nome, $usuario, $mac, $senha, $cpf, $cidade, $estado, $celular, $aceite){
		$valida = $this->verificaNovoUsuario($mac, $cpf);
		if($valida){
			$this->bdCadastraCliente($nome, $usuario, $mac, $senha, $cpf, $cidade, $estado, $celular, $aceite);
			return 1;
		} else{
			return "CPF já existente";
		}
	}
	public function verificaNovoUsuario($mac, $cpf){
		$validaMac = $this->bdVerificaMac($mac);
		$validaCPF = $this->bdVerificaClienteCPF($cpf);

		if(!$validaMac && !$validaCPF){
			return 1;
		} else{
			return 0;
		}

	}
	private function verificaAcessoIsNull($id){
		$sql = "SELECT acessos FROM cliente WHERE id = :id";
		$sql->bindValue(":id", $id);
		if($sql->execute()){
			if($sql->rowCount() == 1){
				$retorno = $sql->fetch();
				$retorno = $retorno['acessos'];
					if(!$retorno){
						$sql = "UPDATE cliente SET acessos = '1'";
						$sql = $GLOBALS['pdo']->prepare($sql);
						$sql->bindValue(":id", $id);
							if($sql->execute()){
								return 1;
							}
					} else{
						return 0;
					}
			}
		}
	}
	private function bdAddAcesso($id){
		$retorno = $this->verificaAcessoIsNull($id);
		if(!$retorno){
			$sql = "UPDATE cliente SET acessos = acessos + 1 WHERE id = :id";
			$sql = $GLOBALS['pdo']->prepare($sql);
			$sql->bindValue(":id", $id);
			if($sql->execute()){
				return 1;
		} else{
			return 0;
			}
		} 
	}
	private function bdCadastraCliente($nome, $usuario, $mac, $senha, $cpf, $cidade, $estado, $celular, $aceite){
		$sql = "INSERT INTO cliente (nome, usuario, senha, cpf, cidade, estado, celular, aceite) VALUES(:nome, :usuario, :senha, :cpf, :cidade, :estado, :celular, :aceite)";
		$sql = $GLOBALS['pdo']->prepare($sql);
		$sql->bindValue(":nome", $nome);
		$sql->bindValue(":usuario", $usuario);
		$sql->bindValue(":senha", $senha);
		$sql->bindValue(":cpf", $cpf);
		$sql->bindValue(":cidade", $cidade);
		$sql->bindValue(":estado", $estado);
		$sql->bindValue(":celular", $celular);
		$sql->bindValue(":aceite", $aceite);
		//$sql->execute();
			
		if($sql->execute()){
			$id_cliente = $GLOBALS['pdo']->lastInsertId();
			$sql = "INSERT INTO mac (cliente_id, mac) VALUES (:cliente_id, :mac)";
			$sql = $GLOBALS['pdo']->prepare($sql);
			$sql->bindValue(":cliente_id", $id_cliente);
			$sql->bindValue(":mac", $mac);
			$sql->execute();
		}		

	}
	/*Retorna um array com os clientes do Banco*/
	private function bdListaClientes(){
		$sql = "SELECT * FROM cliente";
		$sql = $GLOBALS['pdo']->prepare($sql);
		$sql->execute();
		if ($sql->rowCount() > 0) {
			return $sql->fetchAll();
		} else{
			return "Nenhum cliente Registrado";
		}
	}
	/*Re*/
	private function bdVerificaClienteCPF($cpf){
		$sql = "SELECT * FROM cliente WHERE cpf = :cpf";
		$sql = $GLOBALS['pdo']->prepare($sql);
		$sql->bindValue(":cpf", $cpf);
		$sql->execute();
		if($sql->rowCount() == 1){
			$retorno = $sql->fetch();
			return $retorno;
		} else{
			return 0;
		}
	}
	private function bdVerificaClienteMac($mac){
			$retorno = $this->bdVerificaMac($mac);
			if($retorno != 0){
				$mac_db = $retorno['mac'];
				$id_mac = $retorno['cliente_id'];
				$sql = "SELECT * FROM cliente WHERE id = :id";
				$sql = $GLOBALS['pdo']->prepare($sql);
				$sql->bindValue(":id", $id_mac);
				$sql->execute();
				if ($sql->rowCount() == 1){
					$retorno = $sql->fetch();
					return $retorno;
				} else{
					return 0;
				}
			}
	}
	/*Retorna array com os dados do cliente no banco ou 0 caso não esteja no banco, passando o Mac a ser buscado como parametro*/
	private function bdVerificaMac($mac){
		$sql = "SELECT * FROM mac WHERE mac = :mac";
		$sql = $GLOBALS['pdo']->prepare($sql);
		$sql->bindValue(":mac", $mac);
		$sql->execute();
		if ($sql->rowCount() == 1) {
			$retorno = $sql->fetch();
			return $retorno;
		} else{
			return 0;
		}
	}
}



  /*
        primeiro foreach varre cada equipamento conectado, segundo foreach percorre cada index do array de equipamento, após segundo foreach entra na condição caso exista o ip setado na variavel $ip nesse equipamento e armazena o ip do equipamento na variavel $ipadd, após o terceiro foreach pega percorre o mesmo equipamento e guarda o mac., 
    */
/*function macAddress($retorno, $ip){
	        foreach ($retorno as $dispositivo) {
	            foreach ($dispositivo as $item => $valor_item) {
	                   if($valor_item == $ip){
	                    $ipadd = $valor_item;
	                    foreach ($dispositivo as $item2 => $valor_item2) {
	                       if($item2 == "mac-address"){
	                        $mcad = $valor_item2;
	                       }
	                    }
	                
	                }
	            
	            }
	        }
	        return $mcad;
	    }


	    function teste ($ip, $user_rb, $pass_rb){
			$nasname = "186.250.168.241";

			$API = new routeros_api();

                $API->debug = FALSE;
                if ($API->connect($nasname, $user_rb, $pass_rb)) {

                        $API->write("/ip/hotspot/host/print");
                     
                        $ARRAY = $API->read();
                       
                        $mcad = macAddress($ARRAY, $ip);
  				
       	
           $API->disconnect();
                }

	    	return $mcad;
	    }
/**//*
function verificaCliente($mac){
		
	$sql = "SELECT * FROM cliente WHERE cliente = :mac";
	$sql = $pdo->$prepare($sql);
	$sql->bindValue(":mac", $mac);
	$sql->execute();

	if($sql->rowCount() == 1){
		$cliente = $sql->fetch();
		$mac_db = $cliente['mac'];
		if($mac == $mac_db){
			return 1;
		} else{
			return 0;
		}
	} else{
		return 0;
	}
}
*/

?>
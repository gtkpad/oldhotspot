<?php
// include('include/routeros_api.class_v1.6.php');
function limpavar($var)
{
//$var = preg_replace('/[^[:alnum:]_]/', '',$var);
	$var = preg_replace("/[^0-9,]/", "", $var);
$var = trim($var);//limpa espaços vazio
$var = strip_tags($var);//tira tags html e php
$var = addslashes($var);//Adiciona barras invertidas a uma string
return $var;
}

function limpavarstr($var){
	$var = preg_replace('/[^[:alnum:]_\:]/', '',$var);
//$var = preg_replace("/[^0-9]/", "",$var);
$var = trim($var);//limpa espaços vazio
$var = strip_tags($var);//tira tags html e php
$var = addslashes($var);//Adiciona barras invertidas a uma string
return $var;
}

function limpavaremail($var){
	$var = preg_replace('/[^[:alnum:]_\@\.]/', '',$var);
//$var = preg_replace("/[^0-9]/", "",$var);
$var = trim($var);//limpa espaços vazio
$var = strip_tags($var);//tira tags html e php
$var = addslashes($var);//Adiciona barras invertidas a uma string
return $var;
}

function limpavaruser($texto){
	$texto = strtolower($texto);

	$de = array('Á','Í','Ó','Ú','É','Ä','Ï','Ö','Ü','Ë','À','Ì','Ò','Ù','È','Ã','Õ','Â','Î','Ô','Û','Ê','á','í','ó','ú','é','ä','ï','ö','ü','ë','à','ì','ò','ù','è','ã','õ','â','î','ô','û','ê','Ç','ç',' ');

	$para = array('A','I','O','U','E','A','I','O','U','E','A','I','O','U','E','A','O','A','I','O','U','E','a','i','o','u','e','a','i','o','u','e','a','i','o','u','e','a','o','a','i','o','u','e','C','c','');

	return preg_replace("/[^a-zA-Z0-9_-]/", "", str_replace($de,$para,$texto));

}

function limpavarstrname($var){
	$var = preg_replace('/[^[:alnum:]_]/', '',$var);
//$var = preg_replace("/[^0-9]/", "",$var);
// $var = trim($var);//limpa espaços vazio
 	$var = strip_tags($var);//tira tags html e php
// $var = addslashes($var);//Adiciona barras invertidas a uma string
return $var;
}


?>
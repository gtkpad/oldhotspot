<?php
class Cliente
{
	private $cliente;
	private $pdo;

	public function __construct(){
		$this->pdo = new PDO("mysql:dbname=bc_brainstorm;host=localhost", "gabriel", "G@BrI&L");
		// $this->pdo = new PDO("mysql:dbname=bc_brainstorm;host=localhost", "bc_brainstorm", "eRgWyvldaQ");
	}

	/*Busca cliente no banco de dados pelo mac informado*/
	public function buscaClienteMac($mac){
		$valida = $this->bdVerificaMac($mac);
		if($valida != 0){
			$retorno = $this->bdVerificaClienteMac($mac);
			// $this->cliente = $retorno; 
			return $retorno;		
		} else{
			return false;
		}

	}

	/*Verifica se o mac infomado existe na tabela mac do banco de dados*/
	public function verificaMac($mac){
		
		if($this->bdVerificaMac($mac) != 0){
			return 1;
		} else{
			return 0;
		}
		
	}
	/*Usa a o metodo verificaNovoUsuario para saber se mac cpf e email já existam no banco de dados, caso não exista, insere um novo usuário no banco*/
	public function cadastraCliente($nome, $email, $usuario, $mac, $senha, $cpf, $cidade, $estado, $celular, $aceite){
		$valida = $this->verificaNovoUsuario($mac, $cpf, $email);
		if($valida){
			$this->bdCadastraCliente($nome, $email, $usuario, $mac, $senha, $cpf, $cidade, $estado, $celular, $aceite);
			return 1;
		} else{
			return "CPF já existente";
		}
	}
	/*verifica se o mac e cpf informados existem no banco de dados*/
	public function verificaNovoUsuario($mac, $cpf, $email){
		$validaMac = $this->bdVerificaMac($mac);
		$validaCPF = $this->bdVerificaClienteCPF($cpf);
		$validaEmail = $this->bdVerificaEmail($email);

		if(!$validaMac && !$validaCPF && !$validaEmail){
			return 1;
		} else{
			if ($validaCPF != 0){
				$validaCPF = 1;
			}
			if ($validaEmail != 0){
				$validaEmail = 1;
			}
			if ($validaMac != 0){
				$validaMac = 1;
			}
			return array('cpf' => $validaCPF, 'mac' => $validaMac, 'email' =>$validaEmail);
		}

	}

	private function bdVerificaEmail($email){
		$sql = "SELECT * FROM cliente WHERE email = :email";
		$sql = $this->pdo->prepare($sql);
		$sql->bindValue(":email", $email);
		$sql->execute();
		if($sql->rowCount() == 1){
			return $sql->fetch();
		} else{
			return 0;
		}
	}
	public function verificaMacBloqueado($mac){
		if(!empty($mac) && strlen($mac) == 17){
			$sql = "SELECT * FROM mac WHERE mac = :mac";
			$sql = $this->pdo->prepare($sql);
			$sql->bindValue(":mac", $mac);
			$sql->execute();
			if($sql->rowCount() == 1){
				$sql = $sql->fetch();
				if ($sql['bloqueado'] == 1){
					return true;
				} else{
					return false;
				}
			} else{
				return true;
			}
		} else{
			return true;
		}
	}
	public function bdAddMacExtra($mac, $cpf){
		$retorno = $this->bdVerificaClienteCPF($cpf);
		if($retorno != 0){
			$cliente_id = $retorno['id'];
			$sql = "INSERT INTO mac (cliente_id, mac) VALUES (:cod, :mac)";
			$sql = $this->pdo->prepare($sql);
			$sql->bindValue(":cod", $cliente_id);
			$sql->bindValue(":mac", $mac);
			if($sql->execute()){
				return true;
			} else{
				return false;
			}
		} else{
			return false;
		}
	}

	/*Incrementa a coluna acessos da tabela cliente*/
	public function verificaAcessoIsNull($id){
		$sql = "SELECT * FROM cliente WHERE id = :id";
		$sql = $this->pdo->prepare($sql);
		$sql->bindValue(":id", $id);
		$sql->execute();
		if($sql->rowCount() == 1){
			$retorno = $sql->fetch();
			$valida = $retorno['acessos'];
			if(empty($retorno)){
				$sql = "UPDATE cliente SET acessos = 1 WHERE id = :id";
				$sql = $this->pdo->prepare($sql);
				$sql->bindValue(":id", $id);
				$sql->execute();

			} else{
				$sql = "UPDATE cliente SET acessos = acessos + 1 WHERE id = :id";
				$sql = $this->pdo->prepare($sql);
				$sql->bindValue(":id", $id);
				$sql->execute();
			}
		} else{
			return 0;
		}
	}

	// private function bdAddAcesso($id){
	// 	$retorno = $this->verificaAcessoIsNull($id);
	// 	if(!$retorno){
	// 		$sql = "UPDATE cliente SET acessos = acessos + 1 WHERE id = :id";
	// 		$sql = $this->pdo->prepare($sql);
	// 		$sql->bindValue(":id", $id);
	// 		if($sql->execute()){
	// 			return 1;
	// 		} else{
	// 			return 0;
	// 		}
	// 	} 
	// }

	/*cadastra novo cliente no banco na tabela cliente e o mac do dispositivo na tabela mac*/
	private function bdCadastraCliente($nome, $email, $usuario, $mac, $senha, $cpf, $cidade, $estado, $celular, $aceite){
		$sql = "INSERT INTO cliente (nome, email, usuario, senha, cpf, cidade, estado, celular, aceite) VALUES(:nome, :email, :usuario, :senha, :cpf, :cidade, :estado, :celular, :aceite)";
		$sql = $this->pdo->prepare($sql);
		$sql->bindValue(":nome", $nome);
		$sql->bindValue(":email", $email);
		$sql->bindValue(":usuario", $usuario);
		$sql->bindValue(":senha", $senha);
		$sql->bindValue(":cpf", $cpf);
		$sql->bindValue(":cidade", $cidade);
		$sql->bindValue(":estado", $estado);
		$sql->bindValue(":celular", $celular);
		$sql->bindValue(":aceite", $aceite);
		//$sql->execute();

		if($sql->execute()){
			$id_cliente = $this->pdo->lastInsertId();
			$sql = "INSERT INTO mac (cliente_id, mac) VALUES (:cliente_id, :mac)";
			$sql = $this->pdo->prepare($sql);
			$sql->bindValue(":cliente_id", $id_cliente);
			$sql->bindValue(":mac", $mac);
			$sql->execute();
		}		

	}
	/*Retorna um array com os clientes do Banco*/
	private function bdListaClientes(){
		$sql = "SELECT * FROM cliente";
		$sql = $this->pdo->prepare($sql);
		$sql->execute();
		if ($sql->rowCount() > 0) {
			return $sql->fetchAll();
		} else{
			return "Nenhum cliente Registrado";
		}
	}
	/*Retorna o o cliente se o cpf existir no banco*/
	private function bdVerificaClienteCPF($cpf){
		$sql = "SELECT * FROM cliente WHERE cpf = :cpf";
		$sql = $this->pdo->prepare($sql);
		$sql->bindValue(":cpf", $cpf);
		$sql->execute();
		if($sql->rowCount() == 1){
			$retorno = $sql->fetch();
			return $retorno;
		} else{
			return 0;
		}
	}
	/*Retorna o o cliente se o id existir no banco*/
	public function bdVerificaClienteId($id){
		$sql = "SELECT * FROM cliente WHERE id = :id";
		$sql = $this->pdo->prepare($sql);
		$sql->bindValue(":id", $id);
		$sql->execute();
		if($sql->rowCount() == 1){
			$retorno = $sql->fetch();
			return $retorno;
		} else{
			return 0;
		}
	}
	/*Retorna o cliente se o mac vinculado existir no banco*/
	private function bdVerificaClienteMac($mac){
		$retorno = $this->bdVerificaMac($mac);
		if($retorno != 0){
			$mac_db = $retorno['mac'];
			$id_mac = $retorno['cliente_id'];
			$sql = "SELECT * FROM cliente WHERE id = :id";
			$sql = $this->pdo->prepare($sql);
			$sql->bindValue(":id", $id_mac);
			$sql->execute();
			if ($sql->rowCount() == 1){
				$retorno = $sql->fetch();
				return $retorno;
			} else{
				return 0;
			}
		}
	}
	/*Retorna array com os dados do cliente no banco ou 0 caso não esteja no banco, passando o Mac a ser buscado como parametro*/
	public function bdVerificaMac($mac){
		$sql = "SELECT * FROM mac WHERE mac = :mac";
		$sql = $this->pdo->prepare($sql);
		$sql->bindValue(":mac", $mac);
		$sql->execute();
		if ($sql->rowCount() == 1) {
			$retorno = $sql->fetch();
			return $retorno;
		} else{
			return 0;
		}
	}

}

?>
<?php
class Log extends Mikrotik{
	private $pdo;
	private $cliente;

	public function __construct($ip){
		$this->pdo = new PDO("mysql:dbname=bc_brainstorm;host=localhost", "gabriel", "G@BrI&L");
		// $this->pdo = new PDO("mysql:dbname=bc_brainstorm;host=localhost", "bc_brainstorm", "eRgWyvldaQ");
		$this->cliente = new Cliente;
		$this->start($ip);
	}



	
	/*Função que é executada quando a RB envia um post para o arquivo log.php, pega as conexões ativas no banco e verifica se ainda está ativo na RB, se estiver atualiza trafego e uptime, se não tiver atualiza uptime, coloca como inativo no banco de dados e remove da RB*/
	public function verificaConexao(){

		$data = $this->bdListaConexaoAtiva($this->empresa_id);
		if($data != 0 && !empty($data)){
			foreach ($data as $conexao) {
				$mac = $conexao['mac'];
				$id = $conexao['cliente_id'];
				$cliente = $this->cliente->bdVerificaClienteId($id);
				$user = $cliente['usuario'];
				$ativo_rb = $this->verificaActiveMacRb($user, $mac);
				if ($ativo_rb) {
					$this->atualizaUptime($mac, $user, true);
					$this->atualizaTrafego($mac, $user);

				} else{
					$this->atualizaUptime($mac, $user, false);
					$this->setInativo($mac, $user);
					$this->apiRemoveUsers($mac, $user);
					$this->apiRemoveCookie($mac, $user);
				}
				
				
				// echo $mac."\n".$id."\n".$user;
			}
		}
	}

	public function atualizaUptime($mac, $user, $conectado){
		if($conectado){
			$users = $this->apiGetActiveInfo();
		} else{
			$users = $this->apiGetUsersInfo();
		}
		$uptime = $this->getInfoArray($users, $user."@".$mac, "uptime");
		$sql = "SELECT * FROM log WHERE mac = :mac AND ativo = 1";
		$sql = $this->pdo->prepare($sql);
		$sql->bindValue(":mac", $mac);
		$sql->execute();
		if($sql->rowCount() == 1){
			$data = $sql->fetch();
			$data_inicio = $data['start_connection'];
			$hora = $this->horaMikrotik($uptime);
			$total = $hora['total'];
			$uptime = $hora['hora'].":".$hora['minuto'].":".$hora['segundo'];
			$sql = "UPDATE log SET tempo = :uptime WHERE mac = :mac AND ativo = 1";
			$sql = $this->pdo->prepare($sql);
			$sql->bindValue(":uptime", $uptime);
			$sql->bindValue(":mac", $mac);
			$sql->execute();
		}
	}


	/*atualiza trafego do dispositivo no banco de dados, pega informações da tabela active na RB*/
	private function atualizaTrafego($mac, $user){
		$active = $this->apiGetActiveInfo();
		$down = $this->getInfoArray($active, $user."@".$mac, "bytes-out");
		$up = $this->getInfoArray($active, $user."@".$mac, "bytes-in");		
		$sql = "UPDATE log SET download = :down, upload = :up WHERE mac = :mac AND ativo = 1";
		$sql = $this->pdo->prepare($sql);
		$sql->bindValue(":down", $down);
		$sql->bindValue(":up", $up);
		$sql->bindValue(":mac", $mac);
		$sql->execute();
	}
	/*INSERT de log no banco de dados usado quando o cliente loga no hotspot*/
	private function bdAddConexao($cod_cliente, $mac, $ip_cliente, $ip_rb, $empresa_id){
		$sql = "INSERT INTO log (cliente_id, empresa_id, ip_equip, mac, ip_rb, tempo, ativo) VALUES (:cod_cliente, :empresa_id, :ip_equip, :mac, :ip_rb, 0, 1)";
		$sql = $this->pdo->prepare($sql);
		$sql->bindValue(":cod_cliente", $cod_cliente);
		$sql->bindValue(":empresa_id", $empresa_id);
		$sql->bindValue(":ip_equip", $ip_cliente);
		$sql->bindValue(":mac", $mac);
		$sql->bindValue(":ip_rb", $ip_rb);
		$sql->execute();
	}

	/*Coloca coluna ativo = 0 da tabela log no mac informado que tenha ativo = 1 não valida se está inativo na rb pois é usado  no metodo verificaConexao() onde é chamada após validado*/
	public function setInativo($mac, $user){
		$valido = $this->bdListaConexaoAtiva($this->empresa_id);
		if($valido){
			$id_log = $this->getIdLog($mac);
			$sql = "UPDATE log SET ativo = 0 WHERE mac = :mac AND ativo = 1";
			$sql = $this->pdo->prepare($sql);
			$sql->bindValue(":mac", $mac);
			if($sql->execute()){
				$this->setEndConnection($mac, $user, $id_log);
			} 
			else{
				return 0;
			}		
		}
		
	}
	/*retorna o id do log no banco onde o mac informado tenha ativo = 1*/
	private function getIdLog($mac){
		$sql = "SELECT * FROM log WHERE mac = :mac AND ativo = 1";
		$sql = $this->pdo->prepare($sql);
		$sql->bindValue(":mac", $mac);
		$sql->execute();
		if($sql->rowCount() == 1){
			$retorno = $sql->fetch();
			$retorno = $retorno['id'];
			return $retorno;
		} else{
			return 0;
		}

	}
	/*Coloca o a data e hora da desconexão baseado na soma da hora de conexão + uptime*/
	private function setEndConnection($mac, $user, $id_log){
		$users = $this->apiGetUsersInfo();
		$uptime = $this->getInfoArray($users, $user."@".$mac, "uptime");
		$uptime = $this->horaMikrotik($uptime);
		$end = $uptime['total'];
		$sql = "SELECT * FROM log WHERE id = :id";
		$sql = $this->pdo->prepare($sql);
		$sql->bindValue(":id", $id_log);
		$sql->execute();
		if($sql->rowCount() == 1){
			$value = $sql->fetch();
			$start = $value['start_connection'];
			$start = strtotime($start);
			$end += $start;
			$end = date("Y-m-d H:i:s", $end);
			// return $start;
			$sql = "UPDATE log SET end_connection = :end_connection WHERE id = :id";
			$sql = $this->pdo->prepare($sql);
			$sql->bindValue(":end_connection", $end);
			$sql->bindValue(":id", $id_log);
			$sql->execute();
		}
	}
	
	/*Adiciona log de nova conexão no banco*/
	public function addLog($mac, $empresa_id){
		$retorno = $this->verificaExisteLog($mac, $empresa_id);
		if(!$retorno){
			$cliente = $this->cliente->buscaClienteMac($mac);
			$id = $cliente['id'];
			$address = $this->apiGetAddressInfo();
			$ip_rb = $this->getInfoArray($address, "ether1", "address");
			$hosts = $this->apiGetHostsInfo();
			$ip_cliente = $this->getInfoArray($hosts, $mac, "address");
			$this->bdAddConexao($id, $mac, $ip_cliente, $ip_rb, $empresa_id);
			
		}
	}
	/*Verifica se o mac informado está ativo nos logs*/
	private function verificaExisteLog($mac, $empresa_id){
		$sql = "SELECT * FROM log WHERE mac = :mac AND empresa_id = :empresa_id AND ativo = 1";
		$sql = $this->pdo->prepare($sql);
		$sql->bindValue(":mac", $mac);
		$sql->bindValue(":empresa_id", $empresa_id);
		$sql->execute();
		if($sql->rowCount() > 0){
			return true;
		} else{
			return false;
		}
	}

	private function bdListaConexaoAtiva($empresa_id){
		$sql = "SELECT * FROM log WHERE ativo = 1 AND empresa_id = :empresa_id";
		$sql = $this->pdo->prepare($sql);
		$sql->bindValue(":empresa_id", $empresa_id);
		$sql->execute();
		if($sql->rowCount() > 0){
			$retorno = $sql->fetchAll();
			return $retorno;
		} else{
			return 0;
		}
	}

}

?>
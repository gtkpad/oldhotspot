<?php

class Mikrotik{
	private $api;
	public $nasname;
	public $empresa_id;
    protected $user_rb;
    protected $pass_rb;
    protected $port_rb;
    private $pdo;

    public function __construct($ip){
		$this->start($ip);
	}

	public function start($ip){
		try{
            // $this->pdo = new PDO("mysql:dbname=bc_brainstorm;host=localhost", "bc_brainstorm", "eRgWyvldaQ");
			$this->pdo = new PDO("mysql:dbname=bc_brainstorm;host=localhost", "gabriel", "G@BrI&L");
		}catch(PDOExeption $e){
            echo "Erro: ".$e->getMessage();
            exit;
        }
		$sql = $this->pdo->prepare('SELECT * FROM config WHERE ip_hotspot = :ip');
		$sql->bindValue(":ip", $ip);
        $sql->execute();
        if($sql->rowCount() > 0){
			$data = $sql->fetch();
			$this->empresa_id = $data['id'];
            $this->user_rb = $data['user_rb'];
            $this->pass_rb = $data['pass_rb'];
            $this->nasname = $data['ip_hotspot'];
            $this->port_rb = $data['port_rb'];
        
		}
		$this->api = new routeros_api();
		$this->api->port = $this->port_rb;
	}
	
	/*Cria usuário na RB */
	public function addUserRb($user, $password){
		if ($this->api->connect($this->nasname, $this->user_rb, $this->pass_rb)) {
	 		  
			
				$this->api->write('/ip/hotspot/user/add', false);
				$this->api->write('=name=' . $user . '', false);
				$this->api->write('=password=' . $password . '');
				$this->api->write('=profile=default', false);
				$this->api->read();
	
				$this->api->disconnect();
		}	
	}


		/*filtra dados de um array retornado da api, passar primeiro parâmetro o aray retornado, segundo uma informação que sabe do equipamento e o terceiro parâmetro a informação que quer pegar do array*/
		public function getInfoArray($array, $filtro, $info){
			foreach ($array as $dispositivo) {
				foreach ($dispositivo as $item => $valor_item) {
					if($valor_item == $filtro){
						$macadd = $valor_item;
						foreach ($dispositivo as $item2 => $valor_item2) {
							if($item2 == $info){
								$retorna = $valor_item2;
								return $retorna;
							}
						}
	
					}
	
				}
			}
	
		}
	
	

    protected function horaMikrotik($hora){
		$s = 00;
		$m = 00;
		$h = 00;
		if (!strstr($hora, "h") && strstr($hora, "m") && strstr($hora, "s")) {
			// $hr = preg_replace("h", ":", $hora);
			$hr = str_replace("s", "", $hora);
			$hr = explode("m", $hr);
			$m = $hr[0];
			$s = $hr[1];
			$h = 00;
		} else if (!strstr($hora, "h") && !strstr($hora, "m") && strstr($hora, "s")) {
			$hr = str_replace("s", "", $hora);
			$h = 00;
			$m = 00;
			$s = intval($hr);
		} elseif (!strstr($hora, "h") && !strstr($hora, "m") && !strstr($hora, "s")) {
			$s = 00;
			$m = 00;
			$h = 00;
		} elseif (strstr($hora, "h") && strstr($hora, "m") && !strstr($hora, "s")) {
			$s = 00;
			$hr = str_replace("m", "", $hora);
			$hr = explode("h", $hr);
			$h = $hr[0];
			$m = $hr[1]; 
		} elseif (strstr($hora, "h") && !strstr($hora, "m") && !strstr($hora, "s")) {
			$s = 00;
			$m = 00;
			$hr = str_replace("h", "", $hora);
			$h = intval($hr);
		} elseif (!strstr($hora, "h") && strstr($hora, "m") && !strstr($hora, "s")) {
			$s = 00;
			$h = 00;
			$hr = str_replace("m", "", $hora);
			$m = intval($hr); 
		} elseif (strstr($hora, "h") && !strstr($hora, "m") && strstr($hora, "s")) {
			$m = 00;
			$hr = str_replace("s", "", $hora);
			$hr = explode("h", $hr);
			$h = $hr[0];
			$s = $hr[1]; 
		} elseif (strstr($hora, "h") && strstr($hora, "m") && strstr($hora, "s")) {
			$hr = str_replace("s", "", $hora);;
			$hr2 = explode("h", $hr);
			$h = $hr2[0];
			$hr = str_replace("h", "", $hr);;
			$hr = str_replace($h, "", $hr);;
			$hr2 = explode("m", $hr);
			$s = $hr2[1];
			$m = $hr2[0]; 
		}
		$time = (intval($h) * 3600) + (intval($m) * 60) + intval($s);
		return array('total' => $time, 'hora' => $h, 'minuto' => $m, 'segundo' => $s);
    }
    
    protected function apiGetAddressInfo()
	{
		if($this->api->connect($this->nasname, $this->user_rb, $this->pass_rb)) {

			$array = $this->api->comm("/ip/address/print");

			$this->api->disconnect();

			if(!empty($array)){
				return $array;
			} else{
				return 0;
			}
		}
	}

	/*retorna array com informações de /ip hotspot user print da RB*/
	protected function apiGetUsersInfo()
	{
		if($this->api->connect($this->nasname, $this->user_rb, $this->pass_rb)) {

			$array = $this->api->comm("/ip/hotspot/user/print");

			
			$this->api->disconnect();

			if(!empty($array)){
				return $array;
			} else{
				return 0;
			}
		}
	}
	/*retorna array com informações de /ip hotspot cookie print da RB*/
	protected function apiGetCookieInfo()
	{
		if($this->api->connect($this->nasname, $this->user_rb, $this->pass_rb)) {

			$array = $this->api->comm("/ip/hotspot/cookie/print");

			
			$this->api->disconnect();

			if(!empty($array)){
				return $array;
			} else{
				return 0;
			}
		}
	}
	/*remove cliente da tabela users no hotspot da RB, deve ser passado mac e user do bd porque na RB o usuario fica user@mac*/
	protected function apiRemoveUsers($mac, $user)
	{
		$array = $this->apiGetUsersInfo();
		$id = $this->getInfoArray($array, $user."@".$mac, ".id");
		if($this->api->connect($this->nasname, $this->user_rb, $this->pass_rb)) {

			$this->api->write('/ip/hotspot/user/remove', false);
			$this->api->write('=.id=' . $id);
			$this->api->read();

			$this->api->disconnect();
		}

	}
	/*remove cliente da tabela cookie no hotspot da RB, deve ser passado mac e user do bd porque na RB o usuario fica user@mac*/
	protected function apiRemoveCookie($mac, $user)
	{
		$array = $this->apiGetCookieInfo();
		$id = $this->getInfoArray($array, $user."@".$mac, ".id");
		if($this->api->connect($this->nasname, $this->user_rb, $this->pass_rb)) {

			$this->api->write('/ip/hotspot/cookie/remove', false);
			$this->api->write('=.id=' . $id);
			$this->api->read();

			$this->api->disconnect();
		}

	}
	/*retorna array com informações de /ip hotspot host print da RB*/
	protected function apiGetHostsInfo()
	{
		if($this->api->connect($this->nasname, $this->user_rb, $this->pass_rb)) {

			$array = $this->api->comm("/ip/hotspot/host/print");

			$this->api->disconnect();

			if(!empty($array)){
				return $array;
			} else{
				return 0;
			}
		}
	}
	/*retorna array com informações de /ip active cookie print da RB*/
	protected function apiGetActiveInfo()
	{
		if($this->api->connect($this->nasname, $this->user_rb, $this->pass_rb)) {

			$array = $this->api->comm("/ip/hotspot/active/print");

			$this->api->disconnect();

			if(!empty($array)){
				return $array;
			} else{
				return 0;
			}
		}
	}
	/*retorna 1 se o dispositivo estiver na tabela active da RB*/
	public function verificaActiveMacRb($user, $mac){
		$array = $this->apiGetActiveInfo();
		$retorno = $this->getInfoArray($array, $user."@".$mac, "user");
		if(!empty($retorno)){
			return true;
		} else{
			return false;
		}
	}

}

?>
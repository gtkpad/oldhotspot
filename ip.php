<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type");



require "include/functions.php";
require "include/routeros_api.class_v1.6.php";
require_once "include/classes/mikrotik.class.php";
require_once "include/classes/cliente.class.php";
require_once "include/classes/log.class.php";
$ip = $_SERVER['REMOTE_ADDR'];
$mikrotik = new Mikrotik($ip);

if(isset($_POST['mac']) && !empty($_POST['mac']) && $ip == $mikrotik->nasname){
	
   	$mac = limpavarstr($_POST['mac']);
  	$cliente = new Cliente();
  	$valido = $cliente->verificaMac($mac, $mikrotik->empresa_id);
  	
  	if($valido == 1){
		if(!$cliente->verificaMacBloqueado($mac, $mikrotik->empresa_id)){
			echo "1";
		} else{
			echo "44";
		}
  	} else{
  		echo "0";
  	}


} else{
	header("Location: index.html");
}

?>

<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type");
// header("Content-type: text/html; charset=utf-8");
// header("Content-Type: text/html;  charset=ISO-8859-1",true);
// header('Content-Type: application/json; charset=utf-8');


require 'include/config.inc.php';
require "include/functions.php";
//include('include/routeros_api.class_v1.6.php');
if(isset($_POST['datax']) && !empty($_POST['datax'])){
	$json = $_POST['datax'];
	$mac = limpavarstr($json['mac']);
	$nome = $json['nome'];
	$user = limpavaruser($nome);
	$email = limpavaremail($json['email']);
	$cpf = limpavar($json['cpf']);
	$estado = limpavarstrname($json['estado']);
	$cidade = limpavarstrname($json['cidade']);
	$telefone = limpavar($json['telefone']);
	$cliente = new Cliente();
	$valido = $cliente->verificaNovoUsuario($mac, $cpf);
	
	$json = json_encode(array('mac' => $mac, 'nome' => $nome, 'email' => $email, 'cpf' => $cpf, 'estado' => $estado, 'cidade' => $cidade, 'telefone' => $telefone));

	echo $json;
}

?>
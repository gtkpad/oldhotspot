<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type");
// header("Content-type: text/html; charset=utf-8");
// header("Content-Type: text/html;  charset=ISO-8859-1",true);
// header('Content-Type: application/json; charset=utf-8');


require "include/functions.php";
require "include/routeros_api.class_v1.6.php";
require_once "include/classes/mikrotik.class.php";
require_once "include/classes/cliente.class.php";
require_once "include/classes/log.class.php";
$ip = $_SERVER['REMOTE_ADDR'];
$mikrotik = new Mikrotik($ip);
if(isset($_POST['datax']) && !empty($_POST['datax']) && $_SERVER['REMOTE_ADDR'] == $mikrotik->nasname){
	$cliente = new Cliente();
	$data = $_POST['datax'];
	$cpf = limpavar($data['cpf']);
	$mac = limpavarstr($data['mac']);
	if($cliente->bdAddMacExtra($mac, $cpf)){
		echo json_encode(1);
	} else{
		echo json_encode(0);
	}
} else{
	header("Location: index.html");
}

?>
<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type");
//header('Content-Type: application/json; charset=utf-8');

require "include/functions.php";
require "include/routeros_api.class_v1.6.php";
require_once "include/classes/mikrotik.class.php";
require_once "include/classes/cliente.class.php";
require_once "include/classes/log.class.php";
$ip = $_SERVER['REMOTE_ADDR'];
$mikrotik = new Mikrotik($ip);

if(isset($_POST['cmd']) && !empty($_POST['cmd']) && $_SERVER['REMOTE_ADDR'] == $mikrotik->nasname){
	// $nasname = $iphotspot;
	$log = new Log($ip);
	// $mac = "F0:D7:AA:B6:C2:3D";
	// $user = "gabrielvargaspadilha";
	// $ARRAY = $log->verificaActiveMacRb($user,$mac);

	// $retorno = $log->getInfoArray($ARRAY, $user."@".$mac, "uptime");

	// $la = $log->apiGetAddressInfo();
	$log->verificaConexao();
} else{
	header("Location: index.html");
}

?>